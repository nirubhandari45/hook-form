
import './App.css';
import Hook from './Components/Hook';
import Form from './Components/Form'
import Formik from './Components/Formik';
import Forme from './Components/HookForm';
import HookForm from './Components/HookForm';
function App() {
  return (
    <div className="App">
      <HookForm/>
      <Formik/>
      {/* <Forme/> */}
     {/*  <Form/>
      <Hook/> */}
     {/*  <Accountset/> */}
    </div>
  );
}

export default App;
