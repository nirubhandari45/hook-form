import React from 'react'
import { useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
function HookForm() {

  
    /* const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [entry ,setEntry]=useState([]);

  const submitForm=(e)=>{
    e.preventDefault();
    const newEntry={name:name, email:email, password:password}
    setEntry([...entry,newEntry])
    console.log(entry)
  } */
 
  const values=data=>{
   console.log(data)
  }

  const { register, handleSubmit,formState: { errors }  } = useForm();
  
    return (
 
      <div className="create">
        <h2>Hook Form</h2>
        <form onSubmit={handleSubmit(values)}>
          <input 
          name="fname"
          value={register.fname}
            placeholder="Name"
            {...register("text", { required: "This is required please fill." })}
            autoComplete="off"
            onChange={handleSubmit}
          />
        <ErrorMessage errors={errors} name="text" />
        
          <input
          name="email"
          value={register.email }
          autoComplete="off"
            placeholder="email"
            {...register("email", { required: "This is required please fill."})}
            onChange={handleSubmit}
         />
         
          <input
          type="password"
          name="password"
          value={register.password}
          placeholder="password"
            /* {...register('password',{minLength: { value: 6, message: 'Password must be at least 6 characters long' }})} */
            {...register('password',{pattern: { value: /(?=.*[0-9])/, message: 'must contain some number' }})}
           
            autoComplete="off"
            onChange={handleSubmit}
           />
           <ErrorMessage errors={errors} name="password" />
     {/*          <ErrorMessage
        errors={errors}
        name="password"
        render={({ message }) => <p>{message}</p>}
      /> */}
          <input type="submit" value="submit" />
        </form>
      </div>
    );
}

export default HookForm;
