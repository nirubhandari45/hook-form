import React from 'react'
import {useFormik} from 'formik'
function Formik() {

    const formik=useFormik({
        initialValues:{
            fname:'',
            email:'',
            password:''
        },
        onSubmit:values=>{
            console.log(values)
        },
        validate:values=>{
            let errors={}
            if(!values.fname){
                errors.fname="Required"
            }
            if(!values.email){
                errors.email="Required"
            }
          /*   elseif(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))
            {
                error.email="Invalid email format";
            } */
            if(!values.password){
                errors.password="Required"
            }
          return errors
        }
    })
    
    return (
        <div className="create">
            <h2>Formik</h2>
            <form onSubmit={formik.handleSubmit}>
          <input 
            type="text" 
            name="fname"
             onChange={formik.handleChange}
             value={formik.values.fname}
            placeholder="Name"
            autoComplete="off"
          />
          {formik.errors.fname ? <div>{formik.errors.fname}</div>: null}
          <input
          required
            placeholder="email"
            type="email"
            name="email"
            onChange={formik.handleChange}
            value={formik.values.email}
         />
         {formik.errors.email ? <div>{formik.errors.email}</div>: null}
          <input
          required
            type="password"
            name="password"
            onChange={formik.handleChange}
             value={formik.values.password}
            placeholder="password"
           />
            {formik.errors.password ? <div>{formik.errors.password}</div>: null}
       <button >Submit</button>
        </form>
           
            
        </div>
    )
}

export default Formik;
