import React,{useRef} from 'react'

function Hook() {

    const nameRef= useRef();
    const passRef= useRef();
    const emailRef= useRef();
    function submit(){
        {alert(nameRef.current.value)}
    }
    return (
        <div className="create">
            <h2>Using Ref in Uncontrolled</h2>
            <form>
          <input 
          required
            type="text" 
            ref={nameRef}
            placeholder="Name"
            autoComplete="off"
          />
          <input
          required
            placeholder="email"
            type="email"
            ref={emailRef}
         />
         
          <input
          required
            type="password"
            ref={passRef}
            placeholder="password"
           />
            
          <input type="submit" onClick={submit} value="submit"/>
        </form>
           
            
        </div>
    )
}

export default Hook;
