import React,{useState} from 'react'


function Form() {
    const [name,setName]=useState();
    const[fullname,setFullname]=useState();
    const handleSubmit=(e)=>{

      setName(e.target.value)
    }
    const submit=()=>{
          setFullname(name)
    }
    return (
        <div className="create">
            <h1>Hello {fullname}</h1>
            <input
            type="text"
            onChange={handleSubmit}
            value={name}
            />
            <button onClick={submit}>Submit</button>
        </div>
    )
}

export default Form
